#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <oscore_native/message.h>
#include <oscore_native/test.h>
#include <oscore/protection.h>
#include <oscore/contextpair.h>
#include <oscore/context_impl/primitive.h>
#include <oscore/message.h>

static uint8_t ciphertext[13] = {0x61, 0x2f, 0x10, 0x92, 0xf1, 0x77, 0x6f, 0x1c, 0x16, 0x68, 0xb3, 0x82, 0x5e};

int test_protect_aesccm(int introduce_error)
{
    oscore_msgerr_native_t msgerr;
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},

            .recipient_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                              0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                           0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    // First build the protected message to unprotect, then protect again
    oscore_msg_native_t msg = oscore_test_msg_create();
    msgerr = oscore_msg_native_append_option(msg, 3, (uint8_t*)"localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(msg, 9, (uint8_t*)"\x09\x14", 2);
    assert(!oscore_msgerr_native_is_error(msgerr));

    uint8_t *payload;
    size_t payload_len;
    msgerr = oscore_msg_native_map_payload(msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr) && payload_len >= sizeof(ciphertext));
    memcpy(payload, ciphertext, sizeof(ciphertext));
    payload[0] ^= (introduce_error == 1);
    msgerr = oscore_msg_native_trim_payload(msg, sizeof(ciphertext));
    assert(!oscore_msgerr_native_is_error(msgerr));

    oscore_oscoreoption_t header = {
            .option = (uint8_t*)"\x09\x14",
            .option_length = 2
    };
    oscore_requestid_t request_id = {0};
    oscore_msg_protected_t unprotected = {0};
    unprotected.is_writing = true;

    enum oscore_unprotect_request_result oscerr;
    oscerr = oscore_unprotect_request(msg, &unprotected, header, &secctx, &request_id);
    assert(oscerr == OSCORE_UNPROTECT_REQUEST_OK);
    assert(memcmp(payload, (uint8_t[]){0x01, 0xb3, 0x74, 0x76, 0x31}, 5) == 0);

    enum oscore_protect_request_result oscerr_p;
    oscore_msg_native_set_code(msg, 2);
    oscerr_p = oscore_protect_request(&unprotected, &msg, header, &secctx, &request_id);
    assert(oscerr_p == OSCORE_PROTECT_REQUEST_OK);

    assert(memcmp(payload, ciphertext, sizeof(ciphertext)) == 0);

    oscore_test_msg_destroy(msg);
    return 0;
}

int test_wrap_aesccm() {
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},
            .sender_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                           0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_sequence_number = 0x14,
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    // Prepare CoAP message
    oscore_msgerr_native_t msgerr;
    oscore_msg_native_t coap_msg = oscore_test_msg_create();
    oscore_msg_native_set_code(coap_msg, 0x01);
    msgerr = oscore_msg_native_append_option(coap_msg, 3, (uint8_t *) "localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(coap_msg, 11, (uint8_t*)"tv1", 3);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_trim_payload(coap_msg, 0);
    assert(!oscore_msgerr_native_is_error(msgerr));

    //coap_show_pdu(LOG_ERR, coap_msg);

    // Wrap & Protect message
    oscore_msg_native_t oscore_msg = oscore_test_msg_create();
    enum oscore_protect_request_result result;
    result = oscore_wrap_request(coap_msg, oscore_msg, &secctx);
    assert(result == OSCORE_PROTECT_REQUEST_OK);


    assert(oscore_msg_native_get_code(oscore_msg) == 2);
    oscore_msg_native_optiter_t i_iter;
    oscore_msg_native_optiter_init(oscore_msg, &i_iter);
    uint16_t opt_num;
    const uint8_t *opt_val;
    size_t opt_len;

    // Test all options one after the other

    bool next_ok;
    next_ok = oscore_msg_native_optiter_next(oscore_msg, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 3 && opt_len == 9 && memcmp(opt_val, "localhost", 9) == 0);
    next_ok = oscore_msg_native_optiter_next(oscore_msg, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 9 && opt_len == 2 && memcmp(opt_val, "\x09\x14", 2) == 0);
    next_ok = oscore_msg_native_optiter_next(oscore_msg, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(!next_ok);

    msgerr = oscore_msg_native_optiter_finish(oscore_msg, &i_iter);
    assert(!oscore_msgerr_native_is_error(msgerr));

    uint8_t *payload;
    size_t payload_len = 0;
    oscore_msg_native_map_payload(oscore_msg, &payload, &payload_len);
    assert(payload_len == sizeof(ciphertext));
    assert(memcmp(payload, ciphertext, sizeof(ciphertext)) == 0);
    //coap_show_pdu(LOG_ERR, oscore_msg);

    oscore_test_msg_destroy(coap_msg);
    oscore_test_msg_destroy(oscore_msg);

    return 0;
}

int test_derive() {
    struct oscore_master_context masterctx = {
            .master_secret = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                              0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10},
            .master_secret_len = 16,
            .master_salt = {0x9e, 0x7c, 0xa9, 0x22, 0x23, 0x78, 0x63, 0x40},
            .master_salt_len = 8
    };
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .sender_id_len = 0,
            .recipient_id = {1},
            .recipient_id_len = 1
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };
    assert(oscore_context_derive_keys(&secctx, &masterctx));

    uint8_t recipient_key[16] = {0xff, 0xb1, 0x4e, 0x09, 0x3c, 0x94, 0xc9, 0xca,
                              0xc9, 0x47, 0x16, 0x48, 0xb4, 0xf9, 0x87, 0x10};
    uint8_t sender_key[16] = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                           0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff};
    uint8_t common_iv[13] = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c};
    assert(memcmp(primitive.sender_key, sender_key, 16) == 0);
    assert(memcmp(primitive.recipient_key, recipient_key, 16) == 0);
    assert(memcmp(primitive.common_iv, common_iv, 13) == 0);

    return 0;
}

int testmain(int introduce_error)
{
    return test_protect_aesccm(introduce_error) || test_wrap_aesccm() || test_derive();
}
