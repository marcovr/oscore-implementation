#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <oscore_native/message.h>
#include <oscore_native/test.h>
#include <oscore/protection.h>
#include <oscore/contextpair.h>
#include <oscore/context_impl/primitive.h>
#include <oscore/message.h>

int test_unprotect_chacha(int introduce_error)
{
    oscore_msgerr_native_t msgerr;

    struct oscore_context_primitive primitive = {
        .aeadalg = 24,
        .common_iv = "d\xf0\xbd" "1MK\xe0<'\x0c+\x1c",

        .recipient_id_len = 0,
        .recipient_key = "\xd5" "0\x1e\xb1\x8d\x06xI\x95\x08\x93\xba*\xc8\x91" "A|\x89\xae\t\xdfJ8U\xaa\x00\n\xc9\xff\xf3\x87Q",
    };
    oscore_context_t secctx = {
        .type = OSCORE_CONTEXT_PRIMITIVE,
        .data = (void*)(&primitive),
    };

    // A message from plugtest example 1 with ChaCha algorithm
    oscore_msg_native_t msg = oscore_test_msg_create();
    msgerr = oscore_msg_native_append_option(msg, 9, (uint8_t*)"\x09\x00", 2);
    assert(!oscore_msgerr_native_is_error(msgerr));

    uint8_t *payload;
    size_t payload_len;
    oscore_msg_native_map_payload(msg, &payload, &payload_len);
    assert(payload_len >= 32);
    memcpy(payload, "\x5c\x94\xc1\x29\x80\xfd\x93\x68\x4f\x37\x1e\xb2\xf5\x25\xa2\x69\x3b\x47\x4d\x5e\x37\x16\x45\x67\x63\x74\xe6\x8d\x4c\x20\x4a\xdb", 32);
    payload[0] ^= (introduce_error == 1);
    msgerr = oscore_msg_native_trim_payload(msg, 32);
    assert(!oscore_msgerr_native_is_error(msgerr));

    enum oscore_unprotect_request_result oscerr;
    oscore_msgerr_protected_t oscmsgerr;

    // Uninitialized values to be populated
    oscore_oscoreoption_t header = {0};
    oscore_requestid_t request_id = {0};
    oscore_msg_protected_t unprotected = {0};

    assert(oscore_extract_option(msg, &header));

    oscerr = oscore_unprotect_request(msg, &unprotected, header, &secctx, &request_id);

    assert(oscerr == OSCORE_UNPROTECT_REQUEST_OK);

    assert(oscore_msg_protected_get_code(&unprotected) == 1);

    oscore_msg_protected_optiter_t i_iter;
    oscore_msg_protected_optiter_init(&unprotected, &i_iter);
    uint16_t opt_num;
    const uint8_t *opt_val;
    size_t opt_len;

    // Test all options one after the other

    bool next_ok;
    next_ok = oscore_msg_protected_optiter_next(&unprotected, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 11 && opt_len == 6 && memcmp(opt_val, "oscore", 6) == 0);
    next_ok = oscore_msg_protected_optiter_next(&unprotected, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 11 && opt_len == 5 && memcmp(opt_val, "hello", 5) == 0);
    next_ok = oscore_msg_protected_optiter_next(&unprotected, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 11 && opt_len == 1 && *opt_val == '1');
    next_ok = oscore_msg_protected_optiter_next(&unprotected, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(!next_ok);

    oscmsgerr = oscore_msg_protected_optiter_finish(&unprotected, &i_iter);
    assert(!oscore_msgerr_protected_is_error(oscmsgerr));

    uint8_t *p_payload;
    size_t p_payload_len;
    oscmsgerr = oscore_msg_protected_map_payload(&unprotected, &p_payload, &p_payload_len);
    assert(!oscore_msgerr_protected_is_error(oscmsgerr));
    assert(p_payload_len == 0);
    
    oscore_test_msg_destroy(msg);
    return 0;
}

static uint8_t ciphertext[13] = {0x61, 0x2f, 0x10, 0x92, 0xf1, 0x77, 0x6f, 0x1c, 0x16, 0x68, 0xb3, 0x82, 0x5e};

int test_unprotect_aesccm(int introduce_error)
{
    oscore_msgerr_native_t msgerr;

    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},

            .recipient_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                              0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    // A message from Test Vector 4 with AES-CCM algorithm
    oscore_msg_native_t msg = oscore_test_msg_create();
    msgerr = oscore_msg_native_append_option(msg, 3, (uint8_t*)"localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(msg, 9, (uint8_t*)"\x09\x14", 2);
    assert(!oscore_msgerr_native_is_error(msgerr));

    uint8_t *payload;
    size_t payload_len;
    msgerr = oscore_msg_native_map_payload(msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr) && payload_len >= sizeof(ciphertext));
    memcpy(payload, ciphertext, sizeof(ciphertext));
    payload[0] ^= (introduce_error == 1);
    msgerr = oscore_msg_native_trim_payload(msg, sizeof(ciphertext));
    assert(!oscore_msgerr_native_is_error(msgerr));

    enum oscore_unprotect_request_result oscerr;
    oscore_msgerr_protected_t oscmsgerr;

    // Uninitialized values to be populated
    oscore_oscoreoption_t header = {0};
    oscore_requestid_t request_id = {0};
    oscore_msg_protected_t unprotected = {0};

    assert(oscore_extract_option(msg, &header));

    oscore_context_t *ctx_list[] = {NULL, &secctx};
    size_t ctx_list_len = 2;
    oscore_context_t *ctx = NULL;
    assert(oscore_find_ctx(ctx_list, ctx_list_len, msg, &ctx));
    assert(ctx == &secctx);

    oscerr = oscore_unprotect_request(msg, &unprotected, header, &secctx, &request_id);

    assert(oscerr == OSCORE_UNPROTECT_REQUEST_OK);

    assert(oscore_msg_protected_get_code(&unprotected) == 1);

    oscore_msg_protected_optiter_t i_iter;
    oscore_msg_protected_optiter_init(&unprotected, &i_iter);
    uint16_t opt_num;
    const uint8_t *opt_val;
    size_t opt_len;

    // Test all options one after the other

    bool next_ok;
    next_ok = oscore_msg_protected_optiter_next(&unprotected, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 3 && opt_len == 9 && memcmp(opt_val, "localhost", 9) == 0);
    next_ok = oscore_msg_protected_optiter_next(&unprotected, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 11 && opt_len == 3 && memcmp(opt_val, "tv1", 3) == 0);
    next_ok = oscore_msg_protected_optiter_next(&unprotected, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(!next_ok);

    oscmsgerr = oscore_msg_protected_optiter_finish(&unprotected, &i_iter);
    assert(!oscore_msgerr_protected_is_error(oscmsgerr));

    oscmsgerr = oscore_msg_protected_map_payload(&unprotected, &payload, &payload_len);
    assert(!oscore_msgerr_protected_is_error(oscmsgerr));
    assert(payload_len == 0);

    oscore_test_msg_destroy(msg);
    return 0;
}

int test_unwrap_aesccm() {
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},

            .recipient_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                              0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_sequence_number = 0x14,
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    oscore_msgerr_native_t msgerr;
    oscore_msg_native_t oscore_msg = oscore_test_msg_create();
    oscore_msg_native_set_code(oscore_msg, 0x02);
    msgerr = oscore_msg_native_append_option(oscore_msg, 3, (uint8_t *) "localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(oscore_msg, 9, (uint8_t*)"\x09\x14", 2);
    assert(!oscore_msgerr_native_is_error(msgerr));
    uint8_t *payload;
    size_t payload_len = 0;
    msgerr = oscore_msg_native_map_payload(oscore_msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr) && payload_len > sizeof(ciphertext));
    memcpy(payload, ciphertext, sizeof(ciphertext));
    msgerr = oscore_msg_native_trim_payload(oscore_msg, sizeof(ciphertext));
    assert(!oscore_msgerr_native_is_error(msgerr));

    // Unwrap & Unprotect message
    oscore_msg_native_t coap_msg = oscore_test_msg_create();
    enum oscore_unprotect_request_result result;
    result = oscore_unwrap_request(oscore_msg, coap_msg, &secctx);
    assert(result == OSCORE_UNPROTECT_REQUEST_OK);

    assert(oscore_msg_native_get_code(coap_msg) == 1);
    oscore_msg_native_optiter_t i_iter;
    oscore_msg_native_optiter_init(coap_msg, &i_iter);
    uint16_t opt_num;
    const uint8_t *opt_val;
    size_t opt_len;

    // Test all options one after the other

    bool next_ok;
    next_ok = oscore_msg_native_optiter_next(coap_msg, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 3 && opt_len == 9 && memcmp(opt_val, "localhost", 9) == 0);
    next_ok = oscore_msg_native_optiter_next(coap_msg, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(next_ok && opt_num == 11 && opt_len == 3 && memcmp(opt_val, "tv1", 3) == 0);
    next_ok = oscore_msg_native_optiter_next(coap_msg, &i_iter, &opt_num, &opt_val, &opt_len);
    assert(!next_ok);

    msgerr = oscore_msg_native_optiter_finish(coap_msg, &i_iter);
    assert(!oscore_msgerr_native_is_error(msgerr));

    msgerr = oscore_msg_native_map_payload(coap_msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr));
    assert(payload_len == 0);

    oscore_test_msg_destroy(coap_msg);
    oscore_test_msg_destroy(oscore_msg);

    return 0;
}

int testmain(int introduce_error)
{
    return test_unprotect_chacha(introduce_error) ||
    test_unprotect_aesccm(introduce_error) ||
    test_unwrap_aesccm();
}
