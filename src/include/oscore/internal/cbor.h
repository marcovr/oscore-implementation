#ifndef OSCORE_IMPLEMENTATION_CBOR_H
#define OSCORE_IMPLEMENTATION_CBOR_H

/** @file */

/** @ingroup oscore_internal
 *  @addtogroup oscore_cbor OSCORE internal CBOR API
 *
 *  @brief internal API to encode/decode data in CBOR format
 *
 *  These functions help to deal with CBOR, but are not part of the public
 *  OSCORE API. They are only intended for internal use.
 *
 *  @privatesection
 *
 *  @{
 */

#include <stddef.h>
#include <stdint.h>

/** Enumeration of all defined CBOR major types */
enum oscore_cbor_major {
    CBOR_MAJOR_UINT = 0x00,
    CBOR_MAJOR_NINT = 0x20,
    CBOR_MAJOR_BSTR = 0x40,
    CBOR_MAJOR_TSTR = 0x60,
    CBOR_MAJOR_ARRY = 0x80,
    CBOR_MAJOR_MAP  = 0xA0,
    CBOR_MAJOR_TAG  = 0xC0,
    CBOR_MAJOR_PRIM = 0xE0
};

/** Return the number of bytes needed to (canonically) encode a positive
 * integer of value input, which is also the length of byte string / text
 * string / array headers. The input size must be expressible in CBOR, ie. be
 * expressible in 4 bytes at most. */
size_t oscore_cbor_intsize(size_t input);

/** Encode a number of bytes as a (canonical) CBOR positive integer, subject to
 * the same constraints as cbor_intsize. The type major value is added into the first byte. */
size_t oscore_cbor_intencode(size_t input, uint8_t *buf, enum oscore_cbor_major type);

/** Return the number of bytes needed to (canonically) encode a byte string
 * with length input_len, which is also. The input size must be expressible in
 * CBOR, ie. be expressible in 4 bytes at most. */
size_t oscore_cbor_bstrsize(size_t input_len);

/** Encode a byte string: encode its length, then copy the bytes over.
 * @warning Buffer length is not checked, data needs to fit. */
size_t oscore_cbor_bstrencode(const uint8_t *input, size_t input_len, uint8_t *buf);

/** Decode a CBOR integer and its major type. Returns the number of bytes
 * occupied by said integer. */
size_t oscore_cbor_intdecode(const uint8_t *input, size_t *output, enum oscore_cbor_major *major);

/** @} */

#endif //OSCORE_IMPLEMENTATION_CBOR_H
