@defgroup oscore_api OSCORE APIs

@brief Functions provided by the OSCORE library

<b>How to use:</b>

At first, an OSCORE security context has to be initialized, which can be done by calling
\ref oscore_context_derive_keys. An OSCORE master context is required as argument, which
simply contains the master salt and secret. To obtain these, an EDHOC key exchange
can be completed.

Then, depending on what needs to be done, one of the wrap/unwrap methods can be used.
For example, to protect a CoAP request, \ref oscore_wrap_request can be called, the result of
which can be sent securely. Or to unprotect a received response, \ref oscore_unwrap_response
would be used, producing a normal CoAP response.
