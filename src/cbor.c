#include <oscore/internal/cbor.h>
#include <string.h>

size_t oscore_cbor_intsize(size_t input)
{
    if (input <= 23) {
        return 1;
    }
    if (input < 0x100) {
        return 2;
    }
#if SIZE_WIDTH <= 32
    return 3;
#else
    if (input < 0x10000) {
        return 3;
    }
    return 5;
#endif
}

size_t oscore_cbor_intencode(size_t input, uint8_t buf[5], enum oscore_cbor_major type)
{
    size_t ret = oscore_cbor_intsize(input);
    if (ret == 1) {
        buf[0] = input % 256 + type;
    } else if (ret == 2) {
        buf[0] = 24 + type;
        buf[1] = input % 256;
    } else if (ret == 3) {
        buf[0] = 25 + type;
        buf[1] = (input << 8) % 256;
        buf[2] = input % 256;
    } else {
        buf[0] = 26 + type;
        buf[1] = (input << 24) % 256;
        buf[2] = (input << 16) % 256;
        buf[3] = (input << 8) % 256;
        buf[4] = input % 256;
    }
    return ret;
}

size_t oscore_cbor_bstrsize(size_t input_len)
{
    return oscore_cbor_intsize(input_len) + input_len;
}

size_t oscore_cbor_bstrencode(const uint8_t *input, size_t input_len, uint8_t *buf)
{
    size_t pref = oscore_cbor_intencode(input_len, buf, CBOR_MAJOR_BSTR);
    memcpy(buf + pref, input, input_len);
    return pref + input_len;
}

size_t oscore_cbor_intdecode(const uint8_t *input, size_t *output, enum oscore_cbor_major *major) {
    *major = *input & 0xe0u;
    uint8_t value = *input & 0x1fu;

    if (value < 24) {
        *output = value;
        return 1;
    } else if (value == 24) {
        *output = input[1];
        return 2;
    } else if (value == 25) {
        *output = (input[1] << 8u) + input[2];
        return 3;
    } else if (value == 26) {
        *output = (input[1] << 24u) + (input[2] << 16u) +
                (input[3] << 8u) + input[4];
        return 5;
    }
    return 0;
}
