#include <oscore/protection.h>
#include <oscore/internal/cbor.h>

#include <assert.h>

#include <oscore_native/crypto.h>

/** Take the Partial IV from the OSCORE option and populate @ref
 * oscore_request_t from it. Return true if successful, or false if there was
 * no PartIV in the option.
 *
 * This leaves request unmodified if no PartIV was present in the option. */
bool extract_requestid(const oscore_oscoreoption_t *option, oscore_requestid_t *request)
{
    if (option->option_length == 0) {
        return false;
    }

    size_t n = option->option[0] & 0x7;
    if (n == 0) {
        return false;
    }

    // Checked at instance creation; the assert here helps demonstrate that the
    // following memory access is safe.
    assert(n <= PIV_BYTES);

    request->used_bytes = n;
    // I trust the compiler will zero out the whole struct if that is more
    // efficient as it can see that the rest is overwritten later
    memset(request->partial_iv, 0, PIV_BYTES - n);
    request->is_first_use = false;
    uint8_t *dest = &request->partial_iv[PIV_BYTES - n];
    memcpy(dest, &option->option[1], n);

    return true;
}

struct aad_sizes {
    size_t class_i_length;
    size_t external_aad_length;
    size_t aad_length;
};

/** Determine the size of the complete encoded Encrypt0 objecet that
 * constitutes the AAD of a message.
 *
 * @param[in] secctx Security context from which to get a KID
 * @param[in] requester_role Role in @p secctx that created the request
 * @param[in] request The @ref oscore_requestid_t describing the request_piv
 * @param[in] class_i_source The outer message containing all class I options to be considered for this message
 *
 * @todo Actually use Class I options (currently, it is assumed that there are none)
 */
struct aad_sizes predict_aad_size(
        oscore_context_t *secctx,
        enum oscore_context_role requester_role,
        oscore_requestid_t *request,
        oscore_crypto_aeadalg_t aeadalg,
        oscore_msg_native_t class_i_source
        )
{
    uint8_t *request_kid; // ignored, but get_kid still wants to write somewhere
    size_t request_kid_len;
    oscore_context_get_kid(secctx, requester_role, &request_kid, &request_kid_len);

    struct aad_sizes ret;

    // FIXME gather thsi from class_i_source
    ret.class_i_length = 0;
    (void) class_i_source;

    ret.external_aad_length = \
            1 /* array length 5 */ +
            1 /* oscore version 1 */ +
            1 /* 1-long array of of */ +
            oscore_cbor_intsize(aeadalg < 0 ? -1 - aeadalg : aeadalg) /* FIXME strings? */ +
            oscore_cbor_bstrsize(request_kid_len) + /* request_kid */
            oscore_cbor_bstrsize(request->used_bytes) + /* request_piv */
            oscore_cbor_bstrsize(ret.class_i_length);
    ret.aad_length = \
            1 /* array length 3 */ +
            9 /* "Encrytp0" with length */ +
            1 /* empty string with length */ +
            oscore_cbor_bstrsize(ret.external_aad_length);

    return ret;
}

/** Push the AAD for a given message into the decryption state.
 *
 * @param[inout] state AEAD decryption state
 * @param[in] aad_sizes Predetermined sizes of the various AAD components
 * @param[in] secctx Security context from which to pick the sender role KID
 * @param[in] requester_role Role in @p secctx that created the request
 * @param[in] request The @ref oscore_requestid_t describing the request_piv
 * @param[in] class_i_source The outer message containing all class I options to be considered for this message
 *
 */
oscore_cryptoerr_t feed_aad(
        oscore_crypto_aead_decryptstate_t *state,
        struct aad_sizes aad_sizes,
        oscore_context_t *secctx,
        enum oscore_context_role requester_role,
        oscore_requestid_t *request,
        oscore_crypto_aeadalg_t aeadalg,
        oscore_msg_native_t class_i_source
        )
{
    oscore_cryptoerr_t err;
    uint8_t intbuf[5];

    // array length 3, "Encrypt0", h''
    err = oscore_crypto_aead_decrypt_feed_aad(state, (uint8_t*) "\x83\x68" "Encrypt0" "\x40", 11);
    if (oscore_cryptoerr_is_error(err)) { return err; }

    // full external AAD length
    err = oscore_crypto_aead_decrypt_feed_aad(state, intbuf, oscore_cbor_intencode(aad_sizes.external_aad_length, intbuf, 0x40));
    if (oscore_cryptoerr_is_error(err)) { return err; }

    // external AAD array start, constant OSCORE version 1, array of one element
    err = oscore_crypto_aead_decrypt_feed_aad(state, (uint8_t*) "\x85\x01\x81", 3);
    if (oscore_cryptoerr_is_error(err)) { return err; }

    // Used algorithm
    // FIXME strings?
    err = oscore_crypto_aead_decrypt_feed_aad(state, intbuf, oscore_cbor_intencode(aeadalg < 0 ? -1 - aeadalg : aeadalg, intbuf, aeadalg < 0 ? 0x20 : 0x00));
    if (oscore_cryptoerr_is_error(err)) { return err; }

    // Request KID
    uint8_t *request_kid;
    size_t request_kid_len;
    oscore_context_get_kid(secctx, requester_role, &request_kid, &request_kid_len);

    err = oscore_crypto_aead_decrypt_feed_aad(state, intbuf, oscore_cbor_intencode(request_kid_len, intbuf, 0x40));
    if (oscore_cryptoerr_is_error(err)) { return err; }
    err = oscore_crypto_aead_decrypt_feed_aad(state, request_kid, request_kid_len);
    if (oscore_cryptoerr_is_error(err)) { return err; }

    // Request PIV
    err = oscore_crypto_aead_decrypt_feed_aad(state, intbuf, oscore_cbor_intencode(request->used_bytes, intbuf, 0x40));
    if (oscore_cryptoerr_is_error(err)) { return err; }
    err = oscore_crypto_aead_decrypt_feed_aad(state, &request->partial_iv[PIV_BYTES - request->used_bytes], request->used_bytes);
    if (oscore_cryptoerr_is_error(err)) { return err; }

    // Class I options
    assert(aad_sizes.class_i_length == 0);
    // As long as that holds, the Class I source can be disregarded.
    (void) class_i_source;
    // 0 byte string
    err = oscore_crypto_aead_decrypt_feed_aad(state, (uint8_t*) "\x40", 1);

    return err;
}


/** Build a full IV from a partial IV, a security context pair and a sender
 * role
 *
 * @param[out] iv The output buffer
 * @param[in] partiv The zero-padded partial IV
 * @param[in] secctx The security context pair this is used with
 * @param[in] piv_role The role the creator of the Partial IV has in this security context
 * */
void build_iv(
        uint8_t iv[OSCORE_CRYPTO_AEAD_IV_MAXLEN],
        const uint8_t partiv[PIV_BYTES],
        oscore_context_t *secctx,
        enum oscore_context_role piv_role
        )
{
    size_t iv_len = oscore_crypto_aead_get_ivlength(oscore_context_get_aeadalg(secctx));
    const uint8_t *common_iv = oscore_context_get_commoniv(secctx);

    assert(iv_len >= 7);
    assert(iv_len <= OSCORE_CRYPTO_AEAD_IV_MAXLEN);

    uint8_t *id_piv;
    size_t id_piv_len;
    oscore_context_get_kid(secctx, piv_role, &id_piv, &id_piv_len);

    assert(id_piv_len <= iv_len - 6);

    iv[0] = id_piv_len;
    size_t pad1_len = iv_len - 6 - id_piv_len;
    memset(&iv[1], 0, pad1_len);
    memcpy(&iv[1 + pad1_len], id_piv, id_piv_len);
    memcpy(&iv[iv_len - PIV_BYTES], partiv, PIV_BYTES);

    for (size_t i = 0; i < iv_len; i++) {
        iv[i] ^= common_iv[i];
    }
}

bool oscore_oscoreoption_parse(oscore_oscoreoption_t *out, const uint8_t *input, size_t input_len)
{
    if (input_len != 0) {
        uint8_t header = input[0];
        if (header & 0xe0) {
            // Unknown extension bits
            return false;
        }
        uint8_t n = header & 0x07;
        if (n > PIV_BYTES) {
            // Reserved lengths
            return false;
        }
        size_t tail_start = n + 1;

        if (header & 0x10) {
            // h=1: KID context present
            if (tail_start >= input_len) {
                return false;
            }

            // Not validating its value: KID context is opaque
            tail_start += input[tail_start];
        }

        if (header & 0x08) {
            // k=1: KID present
            if (tail_start > input_len) {
                return false;
            }
            // Not validating its value
        } else {
            if (tail_start != input_len) {
                return false;
            }
        }
    }

    out->option = input;
    out->option_length = input_len;
    return true;
}

// Transforms the source value (Sender Sequence Number) into the partial IV.
static void uint64_to_partial_iv(uint64_t source, uint8_t *piv, size_t *out_size) {
    size_t size = 0;
    int max_offset = 1u << PIV_BYTES;

    // PIV is required to be in Network Byte Order (Big Endian).
    // Since the source is most likely stored as Little Endian, memcpy would produce a wrong result.
    // Also, the partial IV can be up to (currently) 5 bytes long, thus htonl() doesn't work.
    for (uint64_t i = max_offset; i >= 8; i -= 8) {
        if (source >= ((uint64_t)1 << i)) {
            piv[size++] = source >> i;
        }
    }
    piv[size++] = source;
    *out_size = size;
}

void oscore_option_build(oscore_oscoreoption_t *option, oscore_context_t *ctx) {
    uint8_t *buffer = (uint8_t *)option->option;
    uint8_t piv[PIV_BYTES] = {0};
    size_t piv_len = 0;
    uint64_t sseqn = oscore_context_getinc_sender_seq_number(ctx);
    uint64_to_partial_iv(sseqn, piv, &piv_len);
    uint8_t *kid;
    size_t kid_len = 0;
    oscore_context_get_kid(ctx, OSCORE_ROLE_SENDER, &kid, &kid_len);

    /*
     *     0 1 2 3 4 5 6 7 <------------- n bytes -------------->
     *    +-+-+-+-+-+-+-+-+--------------------------------------
     *    |0 0 0|h|k|  n  |       Partial IV (if any) ...
     *    +-+-+-+-+-+-+-+-+--------------------------------------
     *
     *     <- 1 byte -> <----- s bytes ------>
     *    +------------+----------------------+------------------+
     *    | s (if any) | kid context (if any) | kid (if any) ... |
     *    +------------+----------------------+------------------+
     *
     */

    // todo: support kid ctx

    if (piv_len == 0) {
        piv_len = 1;
    }
    option->option_length = 1 + piv_len + kid_len;
    buffer[0] = piv_len;
    memcpy(buffer + 1, piv, piv_len);
    buffer[0] |= 1u << 3u;
    memcpy(buffer + 1 + piv_len, kid, kid_len);
}

enum oscore_unprotect_request_result oscore_unprotect_request(
        oscore_msg_native_t protected,
        oscore_msg_protected_t *unprotected,
        oscore_oscoreoption_t header,
        oscore_context_t *secctx,
        oscore_requestid_t *request_id
        )
{
    /* Comparing to the equivalent aiocoap code:
     *
     * * Not asserting anything about the request or response code properties;
     *   instead, all following steps make sure to always look into is_request
     *   and not into the request code class (which is easy here as there's not
     *   even the API to decide that).
     *
     * * Not checking for the validity of the header, that was already done
     *   when it was created.
     *
     * * Not checking whether the given KID and the own recipient ID match;
     *   that's up to the caller, but if the caller errs, we make sure to
     *   always look into the provided security context and not what's in the
     *   header. If things then still work out, fine (the peers probably agree
     *   on some weird form of calling the contexts by a shorter name), but the
     *   assertion we give to the caller that the message was unprotected using
     *   the given context (which is then used to decide authorization) is
     *   upheld.
     */

    oscore_crypto_aeadalg_t aeadalg = oscore_context_get_aeadalg(secctx);
    size_t tag_length = oscore_crypto_aead_get_taglength(aeadalg); 
    size_t minimum_ciphertext_length = 1 + tag_length;

    uint8_t *ciphertext;
    size_t ciphertext_length = 0;
    oscore_msg_native_map_payload(protected, &ciphertext, &ciphertext_length);
    if (ciphertext_length < minimum_ciphertext_length) {
        // Ciphertext too short
        return OSCORE_UNPROTECT_REQUEST_INVALID;
    }
    size_t plaintext_length = ciphertext_length - tag_length; // >= 1

    bool has_request_id = extract_requestid(&header, request_id);
    if (!has_request_id) {
        return OSCORE_UNPROTECT_REQUEST_INVALID;
    }

    struct aad_sizes aad_sizes = predict_aad_size(secctx, OSCORE_ROLE_RECIPIENT, request_id, aeadalg, protected);

    uint8_t iv[OSCORE_CRYPTO_AEAD_IV_MAXLEN];
    build_iv(iv, request_id->partial_iv, secctx, OSCORE_ROLE_RECIPIENT);

    oscore_cryptoerr_t err;
    oscore_crypto_aead_decryptstate_t dec;
    err = oscore_crypto_aead_decrypt_start(
            &dec,
            aeadalg,
            aad_sizes.aad_length,
            plaintext_length,
            iv,
            oscore_context_get_key(secctx, OSCORE_ROLE_RECIPIENT)
            );
    if (!oscore_cryptoerr_is_error(err)) {
        err = feed_aad(&dec, aad_sizes, secctx, OSCORE_ROLE_RECIPIENT, request_id, aeadalg, protected);
    }
    if (!oscore_cryptoerr_is_error(err)) {
        err = oscore_crypto_aead_decrypt_inplace(
                &dec,
                ciphertext,
                ciphertext_length);
    }

    if (oscore_cryptoerr_is_error(err)) {
        return OSCORE_UNPROTECT_REQUEST_INVALID;
    }

    oscore_context_strikeout_requestid(secctx, request_id);

    // FIXME all of that needs to be initialized
    unprotected->backend = protected;
    unprotected->tag_length = tag_length;

    return request_id->is_first_use ? OSCORE_UNPROTECT_REQUEST_OK : OSCORE_UNPROTECT_REQUEST_DUPLICATE;
}

enum oscore_protect_request_result oscore_protect_request(
        oscore_msg_protected_t *unprotected,
        oscore_msg_native_t *protected,
        oscore_oscoreoption_t header,
        oscore_context_t *secctx,
        oscore_requestid_t *request_id
        )
{
    oscore_crypto_aeadalg_t aeadalg = oscore_context_get_aeadalg(secctx);
    size_t tag_length = oscore_crypto_aead_get_taglength(aeadalg);
    size_t minimum_ciphertext_length = 1 + tag_length;
    *protected = unprotected->backend;

    uint8_t *ciphertext;
    size_t ciphertext_length = 0;
    oscore_msg_native_map_payload(*protected, &ciphertext, &ciphertext_length);
    if (ciphertext_length < minimum_ciphertext_length) {
        // Ciphertext too short
        return OSCORE_PROTECT_REQUEST_INVALID;
    }
    size_t plaintext_length = ciphertext_length - tag_length; // >= 1

    bool has_request_id = extract_requestid(&header, request_id);
    if (!has_request_id) {
        return OSCORE_PROTECT_REQUEST_INVALID;
    }

    struct aad_sizes aad_sizes = predict_aad_size(secctx, OSCORE_ROLE_SENDER, request_id, aeadalg, *protected);

    uint8_t iv[OSCORE_CRYPTO_AEAD_IV_MAXLEN];
    build_iv(iv, request_id->partial_iv, secctx, OSCORE_ROLE_SENDER);

    oscore_cryptoerr_t err;
    oscore_crypto_aead_encryptstate_t dec;
    err = oscore_crypto_aead_encrypt_start(
            &dec,
            aeadalg,
            aad_sizes.aad_length,
            plaintext_length,
            iv,
            oscore_context_get_key(secctx, OSCORE_ROLE_SENDER)
    );
    if (!oscore_cryptoerr_is_error(err)) {
        err = feed_aad(&dec, aad_sizes, secctx, OSCORE_ROLE_SENDER, request_id, aeadalg, *protected);
    }

    if (!oscore_cryptoerr_is_error(err)) {
        err = oscore_crypto_aead_encrypt_inplace(
                &dec,
                ciphertext,
                ciphertext_length);
    }

    if (oscore_cryptoerr_is_error(err)) {
        return OSCORE_PROTECT_REQUEST_INVALID;
    }

    return OSCORE_PROTECT_REQUEST_OK;
}

enum oscore_protect_request_result oscore_wrap_request(
        oscore_msg_native_t msg,
        oscore_msg_native_t wrapped,
        oscore_context_t *ctx
        )
{
    oscore_msg_protected_t unprotected = {0};
    unprotected.backend = wrapped;
    unprotected.is_writing = true;
    oscore_msg_native_set_code(wrapped, 2);

    oscore_crypto_aeadalg_t aeadalg = oscore_context_get_aeadalg(ctx);
    unprotected.tag_length = oscore_crypto_aead_get_taglength(aeadalg);

    oscore_oscoreoption_t header;
    header.option = (uint8_t[OSCORE_CRYPTO_AEAD_IV_MAXLEN]){0};
    oscore_option_build(&header, ctx);

    bool oscore_option_set = false;
    uint16_t opt_num;
    const uint8_t *value;
    size_t value_len;
    oscore_msgerr_protected_t res;
    oscore_msg_native_optiter_t iter;
    oscore_msg_native_optiter_init(msg, &iter);
    while (oscore_msg_native_optiter_next(msg, &iter, &opt_num, &value, &value_len)) {
        if (opt_num > OSCORE_OPT_NUM && !oscore_option_set) {
            res = oscore_msg_protected_append_option(&unprotected, OSCORE_OPT_NUM, header.option, header.option_length);
            if (oscore_msgerr_protected_is_error(res)) {
                return OSCORE_PROTECT_REQUEST_INVALID;
            }
            oscore_option_set = true;
        }
        res = oscore_msg_protected_append_option(&unprotected, opt_num, value, value_len);
        if (oscore_msgerr_protected_is_error(res)) {
            return OSCORE_PROTECT_REQUEST_INVALID;
        }
    }
    oscore_msg_native_optiter_finish(msg, &iter);

    if (!oscore_option_set) {
        res = oscore_msg_protected_append_option(&unprotected, OSCORE_OPT_NUM, header.option, header.option_length);
        if (oscore_msgerr_protected_is_error(res)) {
            return OSCORE_PROTECT_REQUEST_INVALID;
        }
    }

    oscore_msg_protected_set_code(&unprotected, oscore_msg_native_get_code(msg));

    uint8_t *payload, *payload_target;
    size_t payload_len = 0, payload_max_len = 0;
    oscore_msgerr_native_t res_n;
    res_n = oscore_msg_native_map_payload(msg, &payload, &payload_len);
    res = oscore_msg_protected_map_payload(&unprotected, &payload_target, &payload_max_len);
    if (oscore_msgerr_native_is_error(res_n) || oscore_msgerr_protected_is_error(res)) {
        return OSCORE_PROTECT_REQUEST_INVALID;
    }

    if (payload_len > payload_max_len) {
        return OSCORE_PROTECT_REQUEST_NOMEM;
    }

    if (payload_len > 0) {
        memcpy(payload_target, payload, payload_len);
    }
    res = oscore_msg_protected_trim_payload(&unprotected, payload_len);
    if (oscore_msgerr_protected_is_error(res)) {
        return OSCORE_PROTECT_REQUEST_INVALID;
    }

    oscore_requestid_t request_id;
    return oscore_protect_request(&unprotected, &wrapped, header, ctx, &request_id);
}

enum oscore_protect_request_result oscore_wrap_response(
        oscore_msg_native_t msg,
        oscore_msg_native_t wrapped,
        oscore_context_t *ctx
        )
{
    enum oscore_protect_request_result res;
    res = oscore_wrap_request(msg, wrapped, ctx);
    oscore_msg_native_set_code(wrapped, 0x44);
    return res;
}

enum oscore_unprotect_request_result oscore_unwrap_request(
        oscore_msg_native_t wrapped,
        oscore_msg_native_t msg,
        oscore_context_t *ctx
        )
{
    // Uninitialized values to be populated
    oscore_oscoreoption_t header = {0};
    oscore_requestid_t request_id;
    oscore_msg_protected_t unprotected = {0};

    if (!oscore_extract_option(wrapped, &header)) {
        return OSCORE_UNPROTECT_REQUEST_INVALID;
    }

    bool is_duplicate = false;
    enum oscore_unprotect_request_result oscerr;
    oscerr = oscore_unprotect_request(wrapped, &unprotected, header, ctx, &request_id);
    if (oscerr != OSCORE_UNPROTECT_REQUEST_OK) {
        if (oscerr == OSCORE_UNPROTECT_REQUEST_DUPLICATE) {
            is_duplicate = true;
        } else {
            return oscerr;
        }
    }

    uint16_t opt_num;
    const uint8_t *value;
    size_t value_len;
    oscore_msgerr_native_t res;
    oscore_msg_protected_optiter_t iter_p;
    oscore_msg_protected_optiter_init(&unprotected, &iter_p);
    while (oscore_msg_protected_optiter_next(&unprotected, &iter_p, &opt_num, &value, &value_len)) {
        res = oscore_msg_native_append_option(msg, opt_num, value, value_len);
        if (oscore_msgerr_native_is_error(res)) {
            return OSCORE_UNPROTECT_REQUEST_INVALID;
        }
    }
    oscore_msg_protected_optiter_finish(&unprotected, &iter_p);

    oscore_msg_native_set_code(msg, oscore_msg_protected_get_code(&unprotected));

    uint8_t *payload, *payload_target;
    size_t payload_len = 0, payload_max_len = 0;
    oscore_msgerr_protected_t res_p;
    res = oscore_msg_native_map_payload(msg, &payload_target, &payload_max_len);
    res_p = oscore_msg_protected_map_payload(&unprotected, &payload, &payload_len);
    if (oscore_msgerr_native_is_error(res) || oscore_msgerr_protected_is_error(res_p)) {
        return OSCORE_UNPROTECT_REQUEST_INVALID;
    }

    if (payload_len > payload_max_len) {
        return OSCORE_UNPROTECT_REQUEST_NOMEM;
    }

    if (payload_len > 0) {
        memcpy(payload_target, payload, payload_len);
    }
    res = oscore_msg_native_trim_payload(msg, payload_len);
    if (oscore_msgerr_native_is_error(res)) {
        return OSCORE_UNPROTECT_REQUEST_INVALID;
    }

    return is_duplicate ?
        OSCORE_UNPROTECT_REQUEST_DUPLICATE : OSCORE_UNPROTECT_REQUEST_OK;
}

enum oscore_unprotect_request_result oscore_unwrap_response(
        oscore_msg_native_t wrapped,
        oscore_msg_native_t msg,
        oscore_context_t *ctx
        )
{
    return oscore_unwrap_request(wrapped, msg, ctx);
}

bool oscore_extract_option(oscore_msg_native_t msg, oscore_oscoreoption_t *header) {
    bool found_oscoreoption = false;
    oscore_msg_native_optiter_t iter;
    oscore_msg_native_optiter_init(msg, &iter);
    while (!found_oscoreoption) {
        uint16_t number;
        const uint8_t *value;
        size_t value_length;
        bool next_exists = oscore_msg_native_optiter_next(msg, &iter, &number, &value, &value_length);
        if (!next_exists) {
            break;
        }
        if (number == OSCORE_OPT_NUM) {
            found_oscoreoption = true;
            bool parsed = oscore_oscoreoption_parse(header, value, value_length);
            if (!parsed) {
                return false;
            }
        }
    }
    oscore_msg_native_optiter_finish(msg, &iter);
    return found_oscoreoption;
}
