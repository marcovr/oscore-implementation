#include <oscore/contextpair.h>
#include <oscore/context_impl/primitive.h>
#include <oscore/internal/cbor.h>
#include <oscore/protection.h>

oscore_crypto_aeadalg_t oscore_context_get_aeadalg(const oscore_context_t *secctx)
{
    switch (secctx->type) {
    case OSCORE_CONTEXT_PRIMITIVE:
        {
            struct oscore_context_primitive *primitive = secctx->data;
            return primitive->aeadalg;
        }
    default:
        abort();
    }
}

void oscore_context_get_kid(
        const oscore_context_t *secctx,
        enum oscore_context_role role,
        uint8_t **kid,
        size_t *kid_len
        )
{
    switch (secctx->type) {
    case OSCORE_CONTEXT_PRIMITIVE:
        {
            struct oscore_context_primitive *primitive = secctx->data;
            if (role == OSCORE_ROLE_RECIPIENT) {
                *kid = primitive->recipient_id;
                *kid_len = primitive->recipient_id_len;
            } else {
                *kid = primitive->sender_id;
                *kid_len = primitive->sender_id_len;
            }
            return;
        }
    default:
        abort();
    }
}

const uint8_t *oscore_context_get_commoniv(const oscore_context_t *secctx)
{
    switch (secctx->type) {
    case OSCORE_CONTEXT_PRIMITIVE:
        {
            struct oscore_context_primitive *primitive = secctx->data;
            return primitive->common_iv;
        }
    default:
        abort();
    }
}
const uint8_t *oscore_context_get_key(
        const oscore_context_t *secctx,
        enum oscore_context_role role
        )
{
    switch (secctx->type) {
    case OSCORE_CONTEXT_PRIMITIVE:
        {
            struct oscore_context_primitive *primitive = secctx->data;
            if (role == OSCORE_ROLE_RECIPIENT)
                return primitive->recipient_key;
            else
                return primitive->sender_key;
        }
    default:
        abort();
    }
}

/** @brief Strike out the left edge number from the replay window */
// Like all context_primitive specifics, this is on the path to refactoring
// once we know what's actually needed where
static void roll_window(struct oscore_context_primitive *ctx) {
    bool left_edge_is_seen = true;
    // This could be phrased more efficiently by using an instruction for
    // counting of the left-most digit ones (typically CLZ), but that's not
    // generally portable, and a smart compiler (nb: currently none of the
    // godbolt ones is) would figure that out anyway.

    // FIXME This definitely needs a unit test (and another look at whether it
    // does the right thing, before that).
    while (left_edge_is_seen) {
        left_edge_is_seen = ctx->replay_window >> 31;
        ctx->replay_window <<= 1;
        ctx->replay_window_left_edge += 1;
    }
}

/** @brief Remove the @par n (>= 1) sequence numbers starting at
 * replay_window_left_edge from the window, rolling on the window in case the
 * next number was already used. */
static void advance_window(struct oscore_context_primitive *ctx, size_t n)
{
    ctx->replay_window_left_edge += n;
    if (n > 32) {
        ctx->replay_window = 0;
        return;
    }
    bool needs_roll = ctx->replay_window & (((uint32_t)1) << (32 - n));
    ctx->replay_window <<= n;
    if (needs_roll) {
        roll_window(ctx);
    }
}

void oscore_context_strikeout_requestid(
        oscore_context_t *secctx,
        oscore_requestid_t *request_id)
{
    switch (secctx->type) {
    case OSCORE_CONTEXT_PRIMITIVE:
        {
            struct oscore_context_primitive *primitive = secctx->data;
            // request_id->partial_iv is documented to always be zero-padded
            int64_t numeric = request_id->partial_iv[4] + \
                              request_id->partial_iv[3] * ((int64_t)1 << 8) + \
                              request_id->partial_iv[2] * ((int64_t)1 << 16) + \
                              request_id->partial_iv[1] * ((int64_t)1 << 24) + \
                              request_id->partial_iv[0] * ((int64_t)1 << 32);

            // We can keep comparing here as all is signed and the possible
            // input magnitudes come nowhere near over-/underflowing
            int64_t necessary_shift = numeric - primitive->replay_window_left_edge - 32;
            if (necessary_shift >= 1) {
                advance_window(primitive, necessary_shift);
            }

            bool is_first;

            if (numeric < primitive->replay_window_left_edge) {
                is_first = false;
            } else if (numeric == primitive->replay_window_left_edge) {
                is_first = true;
                roll_window(primitive);
            } else {
                uint32_t mask = ((uint32_t)1) << (32 - (numeric - primitive->replay_window_left_edge));
                is_first = (mask & primitive->replay_window) == 0;
                primitive->replay_window |= mask;
            }

            request_id->is_first_use = is_first;
            return;
        }
    default:
        abort();
    }
}

uint64_t oscore_context_getinc_sender_seq_number(const oscore_context_t *secctx)
{
    switch (secctx->type) {
        case OSCORE_CONTEXT_PRIMITIVE:
        {
            struct oscore_context_primitive *primitive = secctx->data;
            return primitive->sender_sequence_number++;
        }
        default:
            abort();
    }
}

static oscore_cryptoerr_t derive_value(
        struct oscore_master_context *master_ctx,
        uint8_t *id,
        size_t id_len,
        uint8_t *id_context,
        size_t id_context_len,
        oscore_crypto_aeadalg_t aeadalg,
        uint8_t *type,
        size_t type_len,
        uint8_t *output,
        size_t output_len
        )
{
    uint8_t info_len = 1 + oscore_cbor_bstrsize(id_len) +
            (id_context_len == 0 ? 1 : oscore_cbor_bstrsize(id_context_len)) +
            oscore_cbor_intsize(aeadalg < 0 ? -1 - aeadalg : aeadalg) +
            oscore_cbor_bstrsize(type_len) +
            oscore_cbor_intsize(output_len);
    uint8_t info[info_len];
    uint8_t *p = info;
    *p = 0x85;
    p++;
    p += oscore_cbor_bstrencode(id, id_len, p);
    if (id_context_len > 0) {
        p += oscore_cbor_bstrencode(id_context, id_context_len, p);
    } else {
        *p = 0xf6;
        p++;
    }
    p += oscore_cbor_intencode(aeadalg < 0 ? -1 - aeadalg : aeadalg, p, aeadalg < 0 ? CBOR_MAJOR_NINT : CBOR_MAJOR_UINT);
    p += oscore_cbor_intencode(type_len, p, CBOR_MAJOR_TSTR);
    memcpy(p, type, type_len);
    p += type_len;
    oscore_cbor_intencode(output_len, p, CBOR_MAJOR_UINT);

    return oscore_crypto_hkdf_derive(
            COSE_ALGO_HMAC256,
            master_ctx->master_salt,
            master_ctx->master_salt_len,
            master_ctx->master_secret,
            master_ctx->master_secret_len,
            info,
            info_len,
            output,
            output_len
    );
}

bool oscore_context_derive_keys(oscore_context_t *secctx, struct oscore_master_context *master_ctx)
{
    switch (secctx->type) {
        case OSCORE_CONTEXT_PRIMITIVE:
        {
            struct oscore_context_primitive *primitive = secctx->data;

            size_t key_len = 16; // fixme: get from aeadalg
            size_t iv_len = oscore_crypto_aead_get_ivlength(primitive->aeadalg);
            oscore_cryptoerr_t res;
            res = derive_value(
                    master_ctx, primitive->sender_id, primitive->sender_id_len,
                    (uint8_t *) "", 0, primitive->aeadalg,
                    (uint8_t *) "Key", 3,
                    primitive->sender_key, key_len
            );
            if (oscore_cryptoerr_is_error(res)) {
                return false;
            }
            res = derive_value(
                    master_ctx, primitive->recipient_id, primitive->recipient_id_len,
                    (uint8_t *) "", 0, primitive->aeadalg,
                    (uint8_t *) "Key", 3,
                    primitive->recipient_key, key_len
            );
            if (oscore_cryptoerr_is_error(res)) {
                return false;
            }
            res = derive_value(
                    master_ctx, (uint8_t *) "", 0,
                    (uint8_t *) "", 0, primitive->aeadalg,
                    (uint8_t *) "IV", 2,
                    primitive->common_iv, iv_len
            );
            return !oscore_cryptoerr_is_error(res);
        }
        default:
            abort();
    }
}

static bool extract_kid(
        const uint8_t *input,
        size_t input_len,
        const uint8_t **kid,
        size_t *kid_len
        )
{
    // No need to verify contents, is already done previously in parse_option
    uint8_t header = input[0];
    uint8_t n = header & 0x07;
    size_t tail_start = n + 1;

    if (header & 0x10) {
        tail_start += input[tail_start];
    }

    if (header & 0x08) {
        // k=1: KID present
        *kid = input + tail_start;
        *kid_len = input_len - tail_start;
        return true;
    } else {
        return false;
    }
}

bool oscore_find_ctx(
        oscore_context_t **ctx_list,
        size_t ctx_list_len,
        oscore_msg_native_t msg,
        oscore_context_t **ctx
        )
{
    oscore_oscoreoption_t header = {0};
    if (!oscore_extract_option(msg, &header)) {
        return false;
    }
    const uint8_t *opt_kid;
    size_t opt_kid_len = 0;
    if (!extract_kid(header.option, header.option_length, &opt_kid, &opt_kid_len)) {
        return false;
    }

    uint8_t *kid;
    size_t kid_len = 0;
    for (size_t i = 0; i < ctx_list_len; ++i) {
        if (ctx_list[i] == NULL) continue;
        oscore_context_get_kid(ctx_list[i], OSCORE_ROLE_RECIPIENT, &kid, &kid_len);
        if (opt_kid_len == kid_len && memcmp(opt_kid, kid, opt_kid_len) == 0) {
            *ctx = ctx_list[i];
            return true;
        }
    }
    return false;
}
