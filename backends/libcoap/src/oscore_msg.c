#include <stdio.h>
#include <string.h>

#include <oscore_native/message.h>

uint8_t oscore_msg_native_get_code(oscore_msg_native_t msg)
{
    return msg->code;
}

void oscore_msg_native_set_code(oscore_msg_native_t msg, uint8_t code)
{
    msg->code = code;
}

oscore_msgerr_native_t oscore_msg_native_append_option(
        oscore_msg_native_t msg,
        uint16_t option_number,
        const uint8_t *value,
        size_t value_len
        )
{
    size_t result = coap_add_option(msg, option_number, value_len, value);
    return !result;
}

bool oscore_msgerr_native_is_error(oscore_msgerr_native_t err)
{
    return err != 0;
}

void oscore_msg_native_optiter_init(oscore_msg_native_t msg,
        oscore_msg_native_optiter_t *iter
        )
{
    // No properties of msg go into the iterator setup as long as it needs the
    // is_first property
    (void)msg;

    coap_option_iterator_init(msg, iter, COAP_OPT_ALL);
}

bool oscore_msg_native_optiter_next(
        oscore_msg_native_t msg,
        oscore_msg_native_optiter_t *iter,
        uint16_t *option_number,
        const uint8_t **value,
        size_t *value_len
        )
{
    (void)msg;
    
    coap_opt_t *option = coap_option_next(iter);
    if (option == NULL) {
        return false;
    }
    
    *option_number = iter->type;
    *value = coap_opt_value(option);
    *value_len = coap_opt_length(option);

    return true;
}

oscore_msgerr_native_t oscore_msg_native_optiter_finish(
        oscore_msg_native_t msg,
        oscore_msg_native_optiter_t *iter
        )
{
    // no-op: we didn't allocate anything for iteration
    (void)msg;
    (void)iter;

    // Infallible: Options are parsed and if need be rejected as a message on
    // reception
    return 0;
}

oscore_msgerr_native_t oscore_msg_native_update_option(
        oscore_msg_native_t msg,
        uint16_t option_number,
        size_t option_occurrence,
        const uint8_t *value,
        size_t value_len
        )
{
    coap_opt_iterator_t iter;
    uint8_t *iter_value;

    coap_option_iterator_init(msg, &iter, COAP_OPT_ALL);

    while (true) {
        coap_opt_t *option = coap_option_next(&iter);
        if (option == NULL) {
            return 1;
        }

        if (iter.type == option_number) {
            if (option_occurrence > 0) {
                option_occurrence -= 1;
            } else {
                // Found

                // length was shown to be positive, so it can be cast into
                // the unsigned type safely
                if (value_len != coap_opt_length(option)) {
                    return 1;
                }
                iter_value = (uint8_t *)coap_opt_value(option);

                // Be liberal and accept user provided NULL values for zero-length references
                if (value_len != 0) {
                    memcpy(iter_value, value, value_len);
                }
                return 0;
            }
        }
    }
}

oscore_msgerr_native_t oscore_msg_native_map_payload(
        oscore_msg_native_t msg,
        uint8_t **payload,
        size_t *payload_len
        )
{
    size_t len = 0;
    uint8_t *data;
    int res = coap_get_data(msg, &len, &data);

    if (res == 0) {
        /* fixme: This is a hack to allocate memory for the payload once it's
         *  needed. However it's not a very good solution and can break if a
         *  lot of options are added (msg->alloc_size == msg->used_size)
         *  possible alternatives:
         *  - check for code == 0 and set code always after payload
         *  - add another method: ..._start_payload
         *
         * How it works: msg->data == NULL is checked to avoid allocating a
         * payload where it's set to length 0. However libcoap doesn't set
         * msg->data unless there is data, thus check whether the msg is exactly
         * as large as the allocated buffer. Otherwise it's assumed to be a msg
         * which is currently built and a payload is allocated.
         */
        if (msg->data == NULL && msg->alloc_size > msg->used_size) {
            len = msg->max_size - msg->used_size - 1;
            data = coap_add_data_after(msg, len);
            if (data == NULL) {
                return 1;
            }
        } else {
            data = msg->data;
            len = 0;
        }
    }
    
    *payload = data;
    *payload_len = len;
    
    return 0;
}

oscore_msgerr_native_t oscore_msg_native_trim_payload(
        oscore_msg_native_t msg,
        size_t payload_len
        )
{
    if (msg->data == NULL) {
        if (payload_len == 0) {
            // set data not NULL to indicate empty but set payload
            msg->data = msg->token + msg->used_size - msg->token_length;
            return 0;
        }
        return -1;
    }

    size_t payload_max_len = msg->max_size - (msg->data - msg->token);
    if (payload_len > payload_max_len) {
        return 1;
    }

    msg->used_size = msg->data - msg->token + payload_len;
    return 0;
}
