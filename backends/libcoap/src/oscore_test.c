#include <oscore_native/test.h>

#define MESSAGE_DEFAULT_SIZE 1024

oscore_msg_native_t oscore_test_msg_create(void)
{
    coap_pdu_t *message;
    message = coap_pdu_init(COAP_MESSAGE_CON, 0, 0, MESSAGE_DEFAULT_SIZE);
    return message;
}

void oscore_test_msg_destroy(oscore_msg_native_t message)
{
    coap_delete_pdu(message);
}
