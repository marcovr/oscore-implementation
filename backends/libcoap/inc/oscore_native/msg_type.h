#ifndef NANOCOAP_OSCORE_NATIVE_MSG_TYPE_H
#define NANOCOAP_OSCORE_NATIVE_MSG_TYPE_H

#ifdef ESP_PLATFORM
#include "coap.h"
#else
#include <coap2/coap.h>
#endif

#include <stdbool.h>

typedef coap_pdu_t *oscore_msg_native_t;
typedef coap_opt_iterator_t oscore_msg_native_optiter_t;
typedef ssize_t oscore_msgerr_native_t;

#endif
